# Contactinformatie

Renaud Vancoillie
- renaud.vancoillie@student.howest.be

Stef Lauwaet
- stef.lauwaet@student.howest.be
- steflauwaet@hotmail.com

# OpenAPI Specificatie

Het YAML-bestand toont hoe de API kan aangesproken worden. Merk op dat er een pak endpoints in staan voor eventuele verdere ontwikkeling en die dus niet in productie gebruikt worden. 
De effectief gebruikte endpoints zijn:

* [POST] /sanctum/token
* [GET] /users
* [GET] /products
* [POST] /roles/{role}/login
* [POST] /transactions
* [POST] /events

# Query's

Het beheerplatform maakt gebruikt van een complexe SQL-query om de data op te vragen en naar een Excelbestand te exporteren. Het is echter niet mogelijk om een *eenvoudig* overzicht te tonen van de transactie waarbij enkel het totaalbedrag zichtbaar is **én** de gekoppelde boekhoudkundige code. Een boekhoudkundige code is namelijk gekoppeld aan een bepaald product, waarvan de informatie in een andere tabel staat en dus niet wordt opgenomen in de query. Via gewone SQL-syntax is dit wel mogelijk d.m.v. een subquery in het SELECT-statement. 

## Simpele query

Eén rij per transactie, ongeacht hoeveel producten eraan gekoppeld zijn. Enkel het totaalbedrag van de transactie wordt weergegeven samen met de boekhoudkundige code van het eerste product (we gaan er dus vanuit dat per transactie slechts één bepaald soort product verhandeld wordt: bijvoorbeeld tickets **of** gadgets).

## Detailquery

Het aantal rijen per transactie hangt af van het aantal gekoppelde producten. Zo kunnen er bij één transactie twee verschillende tickettypes verkocht worden. Die worden dan telkens op een nieuwe rij voorgesteld. Om het onderscheid duidelijk te kunnen maken, zijn er extra (berekende) kolommen toegevoegd: *Totaalbedrag transactie* en *Totaalbedrag lijn*. Dit is de query die ook terug te vinden is in de export op het beheerplatform.
